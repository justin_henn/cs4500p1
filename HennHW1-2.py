#Justin Henn
#8/24/2018
#CS4500-E01
#This program is used to take time from a text file and find the difference between the two times
#Sources:
#1. https://stackoverflow.com/questions/18072759/list-comprehension-on-a-nested-list
#2. https://www.geeksforgeeks.org/global-local-variables-python/
#3. https://stackoverflow.com/questions/2823472/is-there-a-method-that-tells-my-program-to-quit
#4. https://stackoverflow.com/questions/21620602/add-leading-zero-python
#5. https://stackoverflow.com/questions/82831/how-do-i-check-whether-a-file-exists

#Imported sys package so could use sys.exit(). The source used for this was 3
#Imported os.path to check if file exists. The source used for this was 5

import sys
import os.path

#Global variable to hold the number of lines in the text file. The source used from this was number 2

linecount = 0

#The exit_program function is used to close the files that were opened and exit the program. 
#This function is ran whenever the program needs to exit. The parameters sent to this function
#is the two text files that are opened for input and output. There is nothing returned by this function
#The source used for the sys.exit was number 3

def exit_program(infile, outfile):

        outfile.close()
        infile.close()
        sys.exit(0)

#The count_lines function is used to count the number of lines in the text file that is used
#for input. The function goes through the file and adds 1 to the global variable linecount
#whenever there is a new line. It then checks to make sure linecount is not 0 which would
#mean it was an empty file. If it is an empty file it will call exit_program and close
#the files and exit the program. The function takes the two text files
#as paramaters and does not return anything. The source used for linecount was number 2

def count_lines(infile, outfile):
        for line in infile:
                global linecount
                linecount = linecount + 1

        if linecount == 0:
                outfile.write("Input file was empty.")
                exit_program(infile, outfile)

#The check_time function is used to validate that the input in the text file is correct
#It first sets the file position to the beginnning using seek. It then starts by reading
#the file line by line. It first checks to make sure the line is the correct size by checking
#if the line length is between 5 and 6. If it is not then it exits the program. Then
#it checks to see if the line length to see if it equals 5 and if it does
#then it checks to see if it is not the last line. The last line is the only line
#that should have a length of 5. If a different line has a length of 5
#then it is exits the program. The input is incorrect for one of the lines. It next sees
#if the length equals 6 and if the 6 character in the line is '\n'. If it is not then it exits
#the program. This is because the last line in the file does not have a '\n' at the end of the line
#while every other line does. This check makes sure a text input such as 23:55a cannot be used as 
#input as that input is not correct. It then checks to make sure the third character is the ':'
#as a time will have that in it. Once it has validated the input it will the split the line 
#by the : and strip the : out and create a list from the times. This split also changes
#the list items to integers. This source used from that is from 
#number 1. Next the function goes through the list and checks to make sure the first part of the list
#item is between 0 and 23(hours), and the second part of the list item is between 0 and 59(minutes).
#This is to verify valid times. The input for this function is the two text files and an empty list
#That is created at the beginning of the program. It does not return anything.


def check_time(infile, outfile, time_list):
        infile.seek(0)
        count = 0 
        for line in infile:
                count = count + 1
                if len(line) < 5 or len(line) > 6:
                        outfile.write("This input was not correct: " + line)
                        exit_program(infile,outfile)
                if len(line) == 5:
                        if count != linecount:
                            outfile.write("This input was not correct: " + line)
                            exit_program(infile,outfile)
                if len(line) == 6:
                        if line[5] != '\n':
                            outfile.write("This input was not correct: " + line)
                            exit_program(infile,outfile)
                if line[2] != ':':
                        outfile.write("This input was not correct: " + line)
                        exit_program(infile,outfile)
                if line[0].isdigit() == False or line[1].isdigit() == False or line[3].isdigit() == False or line[4].isdigit() == False:
                        outfile.write("This input was not correct: " + line)
                        exit_program(infile,outfile)
                time_list.append([int(i) for i in line.strip().split(':')])

        for i in range (0, len(time_list)):
                if time_list[i][0] < 0 or time_list[i][0] > 23:
                        outfile.write("This input was not correct: " + (str)(time_list[i][0]) + ":" + (str)(time_list[i][1]) + "\n")
                        exit_program(infile,outfile)
                if time_list[i][1] < 0 or time_list[i][1] > 59:
                        outfile.write("This input was not correct: " + (str)(time_list[i][0]) + ":" + (str)(time_list[i][1]) + "\n")
                        exit_program(infile,outfile)

        #time_list = [[int(i) for i in x] for x in time_list]

#The print_time function is used to print the times in the format needed in the output file. It takes as parameters the output file, the
#times that were taken from the input file, and the time difference made after subtraction. It then writes the times to the file using the
#format function as when the list items were changed to integers, they lost the leading zeroes. The source for this was number 4. The function
#does not return anything


def print_time(outfile, time1_hours, time1_minutes, time2_hours, time2_minutes, time_diff_hours, time_diff_minutes):
        outfile.write(format(time1_hours, '02') + ":" + format(time1_minutes, '02') + ", " + format(time2_hours, '02') + ":" + format(time2_minutes, '02') +
                      " => " + format(time_diff_hours,'02') + ":" + format(time_diff_minutes,'02') + "\n")

#The time_difference function is used to take the time inputs and subtract them and then send the times to the print_time function to print the times. It first
#checks to see if the linecount is even or odd. It creates a new variable called new_count and sets it to linecount. It then checks to see if linecount is even
#and if not then it sets new_count to linecount - 1. This is to make sure that the time subtraction on runs and even amount of times. It then checks
#to see if new_count is greater than 40. If it is that means there are more than 40 pairs and it should only do the first 40 pairs so it sets it to 40. It then runs a loop up to
#what new_count is set. The loop first checks to see if the counter is even or odd. If it is even then it skips the iteration. This is because it takes two
#items from the list with one iteration, so it actually needs the loop to go through twice everytime it does a time subtraction. If i is odd then it first looks
#to see if the first time(hours) or the second time(hours) is greater. If the first time is greater then it checks to see if the first time(minutes) is lesser than
#the second time(minutes). If it is then the first time "borrows" 60 minutes from the hours portion and subtracts one hour from the hours portion. It does the
#subtraction as absolute values(no negative times) and sends it to the print function. If the minutes inequality is anything else, then it does a regular
#subtraction and prints the times. It then checks to see if the first time(hours) is less than the second time(hours). If it is then it checks to see if
#the second time(minutes) list less than the first time(minutes). If so then the second time "borrows" 60 minutes from the hours and subtracts one hour
#and then does the subtraction and prints. If the inequality for minutes is not true then it does a regular subtraction and prints.
#If none of the hours inequalities are true then it does a regular subtraction and prints. Finally if the linecount was odd then it writes out the odd line
#saying there was not matching time for this. The parameters for this is the list with the separate times and the ouput file. There is no return
#The source for linecount is number 2
        
def time_difference(time_list, outfile):
        global linecount
        new_count = linecount

        if linecount % 2 != 0:       
            new_count = linecount - 1
        if new_count > 40:
            new_count = 40
        for i in range(new_count):
                if i % 2 == 0:
                        continue
                if time_list[i][0] < time_list[i-1][0]:
                        if time_list[i][1] > time_list[i-1][1]:
                                hour_difference = abs(time_list[i][0] - ((time_list[i-1][0])-1))
                                minute_difference = abs(time_list[i][1] - ((time_list[i-1][1])+60))
                                print_time(outfile, time_list[i-1][0], time_list[i-1][1], time_list[i][0], time_list[i][1], hour_difference, minute_difference)
                        else:
                                hour_difference =  abs(time_list[i][0] - time_list[i-1][0])
                                minute_difference = abs(time_list[i][1] - time_list[i-1][1])
                                print_time(outfile, time_list[i-1][0], time_list[i-1][1], time_list[i][0], time_list[i][1], hour_difference, minute_difference)                               
                elif time_list[i][0] > time_list[i-1][0]:
                        if time_list[i][1] < time_list [i-1][1]:
                                hour_difference = abs(time_list[i-1][0] - ((time_list[i][0])-1))
                                minute_difference = abs(time_list[i-1][1] - ((time_list[i][1])+60))
                                print_time(outfile, time_list[i-1][0], time_list[i-1][1], time_list[i][0], time_list[i][1], hour_difference, minute_difference)
                        else:
                                hour_difference = abs(time_list[i][0] - time_list[i-1][0])
                                minute_difference = abs(time_list[i][1] - time_list[i-1][1])
                                print_time(outfile, time_list[i-1][0], time_list[i-1][1], time_list[i][0], time_list[i][1], hour_difference, minute_difference)
                else:
                                hour_difference = abs(time_list[i][0] - time_list[i-1][0])
                                minute_difference = abs(time_list[i][1] - time_list[i-1][1])
                                print_time(outfile, time_list[i-1][0], time_list[i-1][1], time_list[i][0], time_list[i][1], hour_difference, minute_difference)

        if linecount % 2 != 0:
            outfile.write("There was not a matching time for this time: " + format(time_list[new_count][0],'02') +
                          ":" + format(time_list[new_count][1],'02') + "\n")
        if linecount > 40:
            outfile.write("There are more than 20 pairs in the file. Stopping at 20th pair.")

#This is the "main" part of the program where the files are open and the empty list is created. It first checks to see if the file
#exists and if not then it exits. It then calls the functions needed and finally exits the program. The source used was 5
if os.path.isfile("HW1input.txt") == False:
    print("File does not exist")
    sys.exit(0)
initfile = open("HW1input.txt")
initoutfile = open("HW1output.txt", 'w')
time_list = []
count_lines(initfile, initoutfile)
check_time(initfile, initoutfile, time_list)
time_difference(time_list,initoutfile)
exit_program(initfile, initoutfile)